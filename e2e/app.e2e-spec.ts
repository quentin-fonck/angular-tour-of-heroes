import { AppPage } from "./app.po";
import { element, by } from "protractor";

describe("Component interaction test", () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  // http://localhost:4200/HeroInteract
  it("should pass properties to children properly", function() {
    const _heroNames = ["Mr. Nice", "Narco", "Bombasto"];
    const _masterName = "Master";

    // go to the coresponding component
    page.navigateTo("HeroInteract");
    // get app-hero-parent in app.component
    const parent = element.all(by.tagName("app-hero-parent")).get(0);
    // get all app-hero-child components from app-hero-parent
    const heroes = parent.all(by.tagName("app-hero-child"));

    // for each hero name in _heroNames
    for (let i = 0; i < _heroNames.length; i++) {
      // get '{{hero}} says: ' relative to _heroNames index position
      const childTitle = heroes
        .get(i)
        .element(by.tagName("h3"))
        .getText();

      // get '{{hero}} Mr. Nice, am at your service, {{_masterName}}. ' relative to _heroNames index position
      const childDetail = heroes
        .get(i)
        .element(by.tagName("p"))
        .getText();

      // test what we expect from the child component when we pass data from the parent component
      // display correct hero name
      expect(childTitle).toEqual(_heroNames[i] + " says:");
      // display the master property
      expect(childDetail).toContain(_masterName);
    }
  });

  // http://localhost:4200/nameInteract
  it("should display trimmed, non-empty names", function() {
    page.navigateTo("NameInteract");

    const _nonEmptyNameIndex = 0;
    const _nonEmptyName = '"Mr. IQ"';

    const parent = element.all(by.tagName("app-name-parent")).get(0);

    const hero = parent
      .all(by.tagName("app-name-child"))
      .get(_nonEmptyNameIndex);

    const displayName = hero.element(by.tagName("h3")).getText();
    expect(displayName).toEqual(_nonEmptyName);
  });
  it("should replace empty name with default name", function() {
    page.navigateTo("NameInteract");
    const _emptyNameIndex = 1;
    const _defaultName = '"<no name set>"';
    const parent = element.all(by.tagName("app-name-parent")).get(0);
    const hero = parent.all(by.tagName("app-name-child")).get(_emptyNameIndex);

    const displayName = hero.element(by.tagName("h3")).getText();
    expect(displayName).toEqual(_defaultName);
  });

  it("should set expected initial values", function() {
    page.navigateTo("VersionInteract");
    const actual = getActual();

    const initialLabel = "Version 1.23";
    const initialLog = "Initial value of major set to 1, Initial value of minor set to 23";

    expect(actual.label).toBe(initialLabel);
    expect(actual.count).toBe(1);
    expect(actual.logs.get(0).getText()).toBe(initialLog);
  });

  it("should set expected values after clicking 'Minor' twice", function() {
    page.navigateTo("VersionInteract");

    const repoTag = element(by.tagName("app-version-parent"));
    const newMinorButton = repoTag.all(by.tagName("button")).get(0);

    newMinorButton.click().then(function() {
      newMinorButton.click().then(function() {
        const actual = getActual();

        const labelAfter2Minor = "Version 1.25";
        const logAfter2Minor = "minor changed from 24 to 25";

        expect(actual.label).toBe(labelAfter2Minor);
        expect(actual.count).toBe(3);
        expect(actual.logs.get(2).getText()).toBe(logAfter2Minor);
      });
    });
  });

  it("should set expected values after clicking 'Major' once", function() {
    const repoTag = element(by.tagName("app-version-parent"));
    const newMajorButton = repoTag.all(by.tagName("button")).get(1);

    newMajorButton.click().then(function() {
      const actual = getActual();

      const labelAfterMajor = "Version 2.0";
      const logAfterMajor = "major changed from 1 to 2, minor changed from 25 to 0";

      expect(actual.label).toBe(labelAfterMajor);
      expect(actual.count).toBe(4);
      expect(actual.logs.get(3).getText()).toBe(logAfterMajor);
    });
  });

  function getActual() {
    const versionTag = element(by.tagName("app-version-child"));
    const label = versionTag.element(by.tagName("h3")).getText();
    const ul = versionTag.element(by.tagName("ul"));
    const logs = ul.all(by.tagName("li"));

    return { label: label, logs: logs, count: logs.count() };
  }
});

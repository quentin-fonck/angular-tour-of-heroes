import { browser, by, element } from 'protractor';

export class AppPage {
  // navigate to url relative to app root
  navigateTo(url) {
    const URL = (url) ? '/' + url : '/';
    return browser.get(URL);
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}

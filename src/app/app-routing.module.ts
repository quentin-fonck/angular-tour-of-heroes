import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';

import { ContactModule } from '@app/contact/contact.module';

import { HeroesComponent } from '@app/heroes/heroes.component';
import { DashboardComponent } from '@app/dashboard/dashboard.component';
import { HeroDetailComponent } from '@app/hero-detail/hero-detail.component';
import { HeroParentComponent } from '@app/component-interaction/hero-parent.component';
import { NameParentComponent } from '@app/component-interaction/name-parent.component';
import { VersionParentComponent } from '@app/component-interaction/version-parent.component.ts';

const routes: Routes = [
  { path: '', redirectTo: 'contact', pathMatch: 'full'},
  { path: 'crisis', loadChildren: 'app/crisis/crisis.module#CrisisModule' },
  { path: 'heroes', loadChildren: 'app/hero/hero.module.3#HeroModule' },
  // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  // {path: 'dashboard', component: DashboardComponent },
  // { path: 'heroes', component: HeroesComponent },
  { path: 'detail/:id', component: HeroDetailComponent },
  // {path: 'HeroInteract', component: HeroParentComponent },
  // {path: 'NameInteract', component: NameParentComponent },
  // {path: 'VersionInteract', component: VersionParentComponent}
];

@NgModule({
  imports: [
    ContactModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

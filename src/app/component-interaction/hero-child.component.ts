import { Component, Input } from '@angular/core';
import { Hero } from '@app/hero';

@Component({
  selector: 'app-hero-child',
  template: `
    <h3>{{hero.name}} says:</h3>

    <p>{{hero.name}}, at your service, {{masterName}}.</p>
  `,
  styles: []
})
export class HeroChildComponent {
@Input() hero: Hero;
@Input('master') masterName: string;

  constructor() { }

}

import { Directive, ElementRef } from '@angular/core';
import { log } from 'util';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective {

  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'gold';
    console.log(`* highlight called for ${el.nativeElement.tagName}`);
  }
}

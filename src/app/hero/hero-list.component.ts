import { Component }  from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Hero,
         HeroService } from './hero.service';

@Component({
  template: `
    <h3 highlight>Hero List</h3>
    <div *ngFor='let hero of heroes | async'>
      <a (click)="selectHero(hero)">{{hero.id}} - {{hero.name}}</a>
    </div>
    <app-reactive-form [hero]="selectedHero"></app-reactive-form>
  `
})
export class HeroListComponent {
  selectedHero: Hero = new Hero(0, '');
  heroes: Observable<Hero[]>;

  constructor(private heroService: HeroService) {
    this.heroes = this.heroService.getHeroes();
  }

  selectHero(hero: Hero): void {
    this.selectedHero = hero;
  }
}

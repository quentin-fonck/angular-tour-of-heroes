import { Component, OnInit, OnChanges, Input } from '@angular/core';
import {  FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Address, Hero, states, heroes } from './data-model';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit, OnChanges {

  // heroForm = new FormGroup ({
  //   name: new FormControl()
  // });

  @Input('hero') selectedHero: Hero;

  heroForm: FormGroup;
  states = states;

  hero = heroes[0];

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);

    this.heroForm.reset({
      name: this.hero.name,
      address: this.hero.addresses[0] || new Address()
    });

    this.heroForm.setValue({
      name: this.selectedHero.name || '',
      address: new Address()
    });
  }

  createForm() {
    this.heroForm = this.fb.group({
      name: ['', Validators.required],
      address: this.fb.group(new Address()) // <-- a FormGroup with a new address
      // power: '',
      // sidekick: ''
    });
    // this.heroForm.setValue({
    //   name: this.hero.name,
    //   address: this.hero.addresses[0] || new Address()
    // });
  }
}

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '@app/shared/shared.module';
import { ContactRoutingModule } from '@app/contact/contact-routing.module';

import { ContactService } from '@app/contact/contact.service';
import { ContactComponent } from '@app/contact/contact.component';

import { HighlightDirective } from '@app/contact/highlight.directive';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ContactRoutingModule
  ],
  declarations: [
    ContactComponent,
    HighlightDirective
  ],
  providers: [ContactService]
})
export class ContactModule { }

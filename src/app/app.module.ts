import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from '@app/core/core.module';
import { SharedModule } from '@app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { InMemoryDataService } from '@app/in-memory-data.service';

import { AppComponent } from '@app/app.component';
import { HeroesComponent } from '@app/heroes/heroes.component';
import { HeroDetailComponent } from '@app/hero-detail/hero-detail.component';
import { DashboardComponent } from '@app/dashboard/dashboard.component';

import { HeroParentComponent } from '@app/component-interaction/hero-parent.component';
import { HeroChildComponent } from '@app/component-interaction/hero-child.component';

import { NameChildComponent } from '@app/component-interaction/name-child.component';
import { NameParentComponent } from '@app/component-interaction/name-parent.component';

import { VersionParentComponent } from "@app/component-interaction/version-parent.component.ts";
import { VersionChildComponent } from "@app/component-interaction/version-child.component.ts";

import { HeroService } from '@app/hero.service';

import { AppRoutingModule } from "@app/app-routing.module";
import { MessagesComponent } from '@app/messages/messages.component';
import { MessageService } from '@app/message.service';
import { HighlightDirective } from '@app/highlight.directive';

import { HeroSearchComponent } from '@app/hero-search/hero-search.component';
import { ReactiveFormComponent } from '@app/reactive-form/reactive-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent,
    HeroesComponent,
    DashboardComponent,
    MessagesComponent,
    HeroChildComponent,
    HeroParentComponent,
    NameParentComponent,
    NameChildComponent,
    VersionChildComponent,
    VersionParentComponent,
    HighlightDirective,
    HeroSearchComponent
  ],
  imports:      [
    BrowserModule,
    CoreModule.forRoot({ userName: 'Quentin' }),
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    // TODO: Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers:    [HeroService, MessageService],
  bootstrap:    [AppComponent]
})
export class AppModule { }

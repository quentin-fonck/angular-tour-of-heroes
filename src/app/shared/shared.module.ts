import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AwesomePipe } from './awesome.pipe';
import { HighlightDirective } from './highlight.directive';
import { ReactiveFormComponent } from '@app/reactive-form/reactive-form.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    AwesomePipe,
    HighlightDirective,
    ReactiveFormComponent
  ],
  exports: [
    AwesomePipe,
    HighlightDirective,
    ReactiveFormComponent,
    CommonModule,
    FormsModule
  ]
})
export class SharedModule { }

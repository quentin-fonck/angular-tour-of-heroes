import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '@app/core/user.service';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
  @Input() subtitle = '';
  title = 'NgModule';
  user = '';
  constructor(private userService: UserService) {}

  ngOnInit() {
    this.user = this.userService.userName;
  }
}
